﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Domain.CircuitModel;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Factory.Abstraction
{
    /// <summary>
    /// Represents an object that can create <see cref="Circuit"/> instances
    /// based of a <see cref="CircuitSource"/>.
    /// </summary>
    public interface ICircuitFactory
    {
        /// <summary>
        /// Attempts to build a circuit from the given <see cref="CircuitSource"/>.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="ct">
        /// A <see cref="CancellationToken"/>.
        /// </param>
        /// <returns>
        /// A tuple consisting of the built circuit (if no errors were emitted),
        /// a list of errors and a list of warnings.
        /// </returns>
        Task<(Circuit? circuit, ImmutableList<string> errors, ImmutableList<string> warnings)>
            BuildCircuit(CircuitSource source, CancellationToken ct);
    }
}
