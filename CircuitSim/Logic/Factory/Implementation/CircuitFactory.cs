﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.ParsingModel;
using CircuitSim.Logic.Building.Abstraction;
using CircuitSim.Logic.Factory.Abstraction;
using CircuitSim.Logic.Input.Sourcing.Abstraction;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Factory.Implementation
{
    /// <summary>
    /// Impl. for <see cref="ICircuitFactory"/>.
    /// </summary>
    public class CircuitFactory : ICircuitFactory
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ICircuitBuilder circuitBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitFactory"/> class.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        public CircuitFactory(IServiceProvider serviceProvider, ICircuitBuilder circuitBuilder)
        {
            this.serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            this.circuitBuilder = circuitBuilder ?? throw new ArgumentNullException(nameof(circuitBuilder));
        }

        /// <inheritdoc />
        public async Task<(Circuit? circuit, ImmutableList<string> errors, ImmutableList<string> warnings)> BuildCircuit(CircuitSource source, CancellationToken ct)
        {
            var parserType = typeof(ICircuitSourcer<>).MakeGenericType(source.GetType());
            var parser = serviceProvider.GetService(parserType);

            if (parser is null)
            {
                return (null, $"No parser found for source type {source.GetType().Name}.".Enumerate().ToImmutableList(), ImmutableList<string>.Empty);
            }

            var parseMethod = parserType.GetMethods().First(
                x => x.Name == nameof(ICircuitSourcer<FileCircuitSource>.ParseCircuitInputAsync)
                && x.GetParameters().Select(x => x.ParameterType).SequenceEqual(new[] { source.GetType(), typeof(CancellationToken) }));

#nullable disable
            var input = await (parseMethod.Invoke(parser, new object[] { source, ct }) as Task<ImmutableList<CircuitInputToken>>).ConfigureAwait(false);
#nullable restore

            this.circuitBuilder.ApplyInputTokens(input);

            this.circuitBuilder.TryBuildCircuit(out var circuit, out var warnings, out var errors);

            return (circuit, errors.ToImmutableList(), warnings.ToImmutableList());
        }
    }
}
