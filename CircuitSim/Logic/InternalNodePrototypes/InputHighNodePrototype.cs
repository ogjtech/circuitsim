﻿using CircuitSim.Core;

namespace CircuitSim.Logic.InternalNodePrototypes
{
    /// <summary>
    /// The implementation for the INPUT_HIGH node.
    /// </summary>
    public sealed class InputHighNodePrototype : InputNodePrototypeBase, INodePrototype<InputHighNodePrototype>
    {
        public static readonly string NodeToken = "INPUT_HIGH";

        public override bool DefaultValue => true;

        /// <inheritdoc />
        public override string Token => NodeToken;

        /// <inheritdoc />
        public override string FriendlyName => "Input (H)";
    }
}
