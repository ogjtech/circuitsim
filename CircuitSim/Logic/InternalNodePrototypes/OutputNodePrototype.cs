﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.Logic.InternalNodePrototypes
{
    /// <summary>
    /// The implementation for the PROBE node.
    /// </summary>
    public sealed class OutputNodePrototype : INodePrototype<OutputNodePrototype>
    {
        public static readonly string NodeToken = "PROBE";

        /// <inheritdoc />
        public string Token => NodeToken;

        /// <inheritdoc />
        public string FriendlyName => "Probe";

        /// <inheritdoc />
        public int MinInputs => 1;

        /// <inheritdoc />
        public int? MaxInputs => 1;

        /// <inheritdoc />
        public int MinOutputs => 0;

        /// <inheritdoc />
        public int? MaxOutputs => 0;

        /// <inheritdoc />
        public ImmutableArray<bool> CalculateResult(ImmutableArray<bool> inputs, int amountOfOutputs)
            => inputs.Single().Enumerate().ToImmutableArray();
    }
}
