﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.Logic.InternalNodePrototypes
{
    /// <summary>
    /// Base class for internal nodes that handle input.
    /// </summary>
    public abstract class InputNodePrototypeBase : INodePrototype
    {
        /// <inheritdoc />
        public abstract string Token { get; }

        /// <inheritdoc />
        public abstract string FriendlyName { get; }

        /// <inheritdoc />
        public abstract bool DefaultValue { get; }

        /// <inheritdoc />
        public int MinInputs => 0;

        /// <inheritdoc />
        public int? MaxInputs => 0;

        /// <inheritdoc />
        public int MinOutputs => 1;

        /// <inheritdoc />
        public int? MaxOutputs => null;

        /// <inheritdoc />
        public ImmutableArray<bool> CalculateResult(ImmutableArray<bool> inputs, int amountOfOutputs)
        {
            // The application will pass in the user selected value for the input
            // at index 0 of the input array.

            return Enumerable.Range(0, amountOfOutputs).Select(_ => inputs[0]).ToImmutableArray();
        }
    }
}
