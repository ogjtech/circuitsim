﻿using CircuitSim.Core;

namespace CircuitSim.Logic.InternalNodePrototypes
{
    /// <summary>
    /// The implementation for the INPUT_LOW node.
    /// </summary>
    public sealed class InputLowNodePrototype : InputNodePrototypeBase, INodePrototype<InputLowNodePrototype>
    {
        public static readonly string NodeToken = "INPUT_LOW";

        public override bool DefaultValue => false;

        /// <inheritdoc />
        public override string Token => NodeToken;

        /// <inheritdoc />
        public override string FriendlyName => "Input (L)";
    }
}
