﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Logic.Building.Abstraction;
using CircuitSim.Logic.Building.Implementation;
using CircuitSim.Logic.Factory.Abstraction;
using CircuitSim.Logic.Factory.Implementation;
using CircuitSim.Logic.Input.Parsing.Abstraction;
using CircuitSim.Logic.Input.Parsing.Implementation;
using CircuitSim.Logic.Input.Reading.Abstraction;
using CircuitSim.Logic.Input.Reading.Implementation;
using CircuitSim.Logic.Input.Sourcing.Abstraction;
using CircuitSim.Logic.Input.Sourcing.Implementation;
using CircuitSim.Logic.Prototypes.Abstraction;
using CircuitSim.Logic.Prototypes.Implementation;
using CircuitSim.Logic.Simulating.Abstraction;
using CircuitSim.Logic.Simulating.Implementation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CircuitSim.Logic
{
    public static class ServiceRegistrations
    {
        public static void ConfigureServices(HostBuilderContext hbc, IServiceCollection svc)
        {
            _ = hbc;
            svc.AddSingleton<IINodePrototypeRepository, INodePrototypeRepository>();
            svc.AddScoped<IFileReader, FileReader>();
            svc.AddScoped<ITextCircuitParser, AvansTextCircuitParser>();
            svc.AddScoped<ICircuitSimulator, CircuitSimulator>();
            svc.AddScoped<ICircuitSourcer<FileCircuitSource>, FileCircuitSourcer>();
            svc.AddTransient<ICircuitBuilder, CircuitBuilder>();
            svc.AddTransient<ICircuitFactory, CircuitFactory>();
        }
    }
}
