﻿using CircuitSim.Domain.StateModel;

namespace CircuitSim.Logic.Simulating.Extensions
{
    /// <summary>
    /// Contains extension methods for the <see cref="bool"/> type.
    /// </summary>
    public static class BoolExtensions
    {
        /// <summary>
        /// Converts the <see cref="bool"/> value to a <see cref="SignalState"/>.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The signal state.</returns>
        public static SignalState AsSignalState(this bool value) =>
            value switch
            {
                true => SignalState.HIGH,
                false => SignalState.LOW,
            };
    }
}
