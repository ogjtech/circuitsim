﻿using CircuitSim.Domain;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.StateModel;
using CircuitSim.Logic.Simulating.Extensions;
using CircuitSim.Logic.InternalNodePrototypes;
using CircuitSim.Logic.Simulating.Abstraction;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Diagnostics;

namespace CircuitSim.Logic.Simulating.Implementation
{
    /// <summary>
    /// Impl. for <see cref="ICircuitSimulator"/>.
    /// </summary>
    public class CircuitSimulator : ICircuitSimulator
    {
        public CircuitSimulationResult SimulateCircuit(
            Circuit circuit,
            ImmutableDictionary<Guid, SignalState> inputSignalStateOverrides)
        {
            // Flatten the circuit.
            var (flatNodes, flatEdges) = FlattenCircuit(circuit);

            // Calculate the amount of edges connected to each node
            var inputEdgesPerNode = flatEdges
                .Select(x => x.Value)
                .GroupBy(x => x.OutputSide.Id)
                .Select(x => new KeyValuePair<Guid, int>(x.Key, x.Count()))
                .Concat(circuit.InputNodes.Select(x => new KeyValuePair<Guid, int>(x.Key, 1))) // The input nodes are a special case, they always have one input.
                .ToImmutableDictionary();

            // Create the first / current state.
            var currentState = new CircuitState(
                flatEdges.ToImmutableDictionary(x => x.Key, x => SignalState.NOT_EVALUATED),
                circuit.OutputNodes.ToImmutableDictionary(x => x.Key, x => SignalState.NOT_EVALUATED));

            // Create the first set of cursors. These are the input nodes with their (overriden) states
            // as input.

            var currentCursors = circuit.InputNodes
                .Select(kvp => new Cursor(
                    kvp.Value,
                    (inputSignalStateOverrides.TryGetValue(kvp.Key, out var @override)
                        ? @override == SignalState.HIGH
                        : kvp.Value.Prototype.GetType() == typeof(InputHighNodePrototype)).Enumerate().ToImmutableArray()))
                .ToImmutableDictionary(x => x.TargetNode.Id);

            var historicalState = new List<CircuitState> { currentState };

            
            while (!currentState.OutputNodeStates.All(x => x.Value != SignalState.NOT_EVALUATED))
            {
                var (newCursors, newState) = ExecuteStep(
                    currentCursors,
                    currentState,
                    inputEdgesPerNode);

                historicalState.Add(newState);
                currentState = newState;
                currentCursors = newCursors;
            }

            return new CircuitSimulationResult(
                historicalState.ToImmutableList());
        }

        /// <summary>
        /// Executes a step in the simulation process.
        /// </summary>
        /// <param name="circuit">The circuit.</param>
        /// <param name="currentCursors">The current cursors.</param>
        /// <param name="currentCircuitState">The current circuit state.</param>
        /// <returns>
        /// The new states.
        /// </returns>
        private static (ImmutableDictionary<Guid, Cursor> newNodeCursors, CircuitState newState) ExecuteStep(
            ImmutableDictionary<Guid, Cursor> currentCursors,
            CircuitState currentCircuitState,
            ImmutableDictionary<Guid, int> inputCountsPerNode)
        {
            var mutableCursors = currentCursors.ToBuilder();
            var mutableOutputNodeState = currentCircuitState.OutputNodeStates.ToBuilder();
            var mutableEdgeState = currentCircuitState.EdgeStates.ToBuilder();

            currentCursors.ForEach((kvp) =>
            {
                var targetNodeId = kvp.Key;
                var targetNode = kvp.Value.TargetNode;
                var targetNodeUserId = targetNode.UserNodeId;
                var targetInput = kvp.Value.InputSignals;
                var cursor = kvp.Value;
                if (inputCountsPerNode[targetNodeId] > cursor.InputSignals.Length)
                {
                    // If not all inputs of the node have been calculated yet,
                    // this node will be calculated at a future iteration.
                    return;
                }

                // All inputs for the node are known.
                // Calculate the outputs and set the cursors to the next node, if any.

                var outputSignals = cursor.TargetNode.Prototype.CalculateResult(
                    cursor.InputSignals,
                    cursor.TargetNode.OutputEdges.Count);

                cursor.TargetNode.OutputEdges.ForEach((index, edge) =>
                {
                    // Update edge states
                    mutableEdgeState[edge.Identifier] = outputSignals[index].AsSignalState();

                    // Update cursors
                    // Create new / update existing cursor for each edge.
                    if (mutableCursors.TryGetValue(edge.OutputSide.Id, out var value))
                    {
                        var newInputSignals = value.InputSignals.Append(outputSignals[index]).ToImmutableArray();

                        // Replace the value.
                        mutableCursors.Remove(edge.OutputSide.Id);
                        mutableCursors.Add(
                            edge.OutputSide.Id,
                            new Cursor(edge.OutputSide,  newInputSignals));
                    }
                    else
                    {
                        // Add a new value.
                        mutableCursors.Add(
                            edge.OutputSide.Id,
                            new Cursor(
                                edge.OutputSide,
                                outputSignals[index].Enumerate().ToImmutableArray()));
                    }
                });

                // If the current node is an output node, update the output node state.
                // In this case, the output signal array is guaranteed to contain a single element.
                if (currentCircuitState.OutputNodeStates.ContainsKey(cursor.TargetNode.Id))
                {
                    mutableOutputNodeState[cursor.TargetNode.Id] = outputSignals.First().AsSignalState();
                }

                // The own cursor may now be deleted.
                mutableCursors.Remove(kvp.Key);
            });

            return (
                mutableCursors.ToImmutable(),
                currentCircuitState.With(
                    newEdgeStates: mutableEdgeState.ToImmutable(),
                    newOutputNodeStates: mutableOutputNodeState.ToImmutable()));
        }

        /// <summary>
        /// Flattens the graph into two key-value collections: one containing
        /// all the nodes and one containing all the edges.
        /// </summary>
        /// <param name="circuit">The circuit.</param>
        /// <returns>The flattened graph.</returns>
        private static (ImmutableDictionary<Guid, Node> nodes, ImmutableDictionary<Guid, Edge> edges) FlattenCircuit(Circuit circuit)
        {
            // Find all nodes
            var nodes = circuit.InputNodes.Values.SelectMany(FlattenNodeRecursive)
                .GroupBy(n => n.Id)
                .Select(g => g.First())
                .ToImmutableDictionary(x => x.Id);

            Debug.Assert(circuit.OutputNodes.All(kvp => nodes.ContainsKey(kvp.Key)));

            // Get all edges from all nodes
            var edges = nodes.SelectMany(kvp => kvp.Value.OutputEdges)
                .GroupBy(e => e.Identifier)
                .Select(g => g.First())
                .ToImmutableDictionary(e => e.Identifier);

            return (nodes, edges);
        }

        private static IEnumerable<Node> FlattenNodeRecursive(Node node)
            => node.OutputEdges.Select(e => e.OutputSide).SelectMany(FlattenNodeRecursive).Append(node);

        /// <summary>
        /// Represents a cursor during evaluation.
        /// </summary>
        /// <remarks>
        /// This struct is equal by the targeted <see cref="Node"/>.
        /// </remarks>
        private readonly struct Cursor
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Cursor"/> struct.
            /// </summary>
            /// <param name="targetNode">The target node.</param>
            /// <param name="inputSignals">The input signals.</param>
            public Cursor(
                Node targetNode,
                ImmutableArray<bool> inputSignals)
            {
                this.TargetNode = targetNode;
                this.InputSignals = inputSignals;
            }

            /// <summary>
            /// Gets the node targeted by this cursor.
            /// </summary>
            public Node TargetNode { get; }

            /// <summary>
            /// Gets the inputs signals for the node.
            /// </summary>
            public ImmutableArray<bool> InputSignals { get; }

            /// <summary>
            /// Clones this <see cref="Cursor"/>, replacing values if supplied.
            /// </summary>
            /// <param name="newInputSignals">The new input signals.</param>
            /// <returns>The cloned <see cref="Cursor"/>.</returns>
            public Cursor With(
                ImmutableArray<bool>? newInputSignals = null)
                => new Cursor(
                    this.TargetNode,
                    newInputSignals ?? this.InputSignals);

            /// <inheritdoc />
            public override bool Equals(object obj)
                => obj is Cursor cur && object.ReferenceEquals(this.TargetNode, cur.TargetNode);

            /// <inheritdoc />
            public override int GetHashCode() =>
                this.TargetNode?.GetHashCode() ?? 0;
        }
    }
}
