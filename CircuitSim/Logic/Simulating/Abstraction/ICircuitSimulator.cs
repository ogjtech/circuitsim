﻿using CircuitSim.Domain;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.StateModel;
using System;
using System.Collections.Immutable;

namespace CircuitSim.Logic.Simulating.Abstraction
{
    /// <summary>
    /// Represents an object that can simulate a circuit.
    /// </summary>
    public interface ICircuitSimulator
    {
        /// <summary>
        /// Executes the circuit simulation for a given <see cref="Circuit"/>.
        /// The circuit is not validated.
        /// </summary>
        /// <param name="circuit">
        /// The circuit to simulate.
        /// </param>
        /// <param name="inputSignalState">
        /// The overrides for the signal states of the circuit.
        /// </param>
        /// <returns>
        /// The result of the simulation.
        /// </returns>
        CircuitSimulationResult SimulateCircuit(
            Circuit circuit,
            ImmutableDictionary<Guid, SignalState> inputSignalStateOverrides);
    }
}
