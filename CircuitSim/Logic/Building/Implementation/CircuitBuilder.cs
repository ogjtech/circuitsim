﻿using CircuitSim.Core;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.ParsingModel;
using CircuitSim.Logic.Building.Abstraction;
using CircuitSim.Logic.InternalNodePrototypes;
using CircuitSim.Logic.Prototypes.Abstraction;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.NetworkInformation;

namespace CircuitSim.Logic.Building.Implementation
{
    class CircuitBuilder : ICircuitBuilder
    {
        private readonly List<string> warnings = new List<string>();
        private readonly List<string> errors = new List<string>();
        private readonly IINodePrototypeRepository nodePrototypeRepository;
        private readonly Dictionary<string, INodePrototype> nodes = new Dictionary<string, INodePrototype>();
        private readonly Dictionary<string, List<string>> edges = new Dictionary<string, List<string>>();


        public CircuitBuilder(IINodePrototypeRepository prototypeRepository) => this.nodePrototypeRepository = prototypeRepository;

        /// <inheritdoc />
        public ICircuitBuilder AddEdge(string userOutputNodeIdentifier, string userInputNodeIdentifier)
        {
            if (edges.TryGetValue(userOutputNodeIdentifier, out var inputIds))
            {
                if (!inputIds.Contains(userInputNodeIdentifier))
                    inputIds.Add(userInputNodeIdentifier);
            }
            else
            {
                edges.Add(
                    userOutputNodeIdentifier,
                    new List<string> { userInputNodeIdentifier });
            }

            return this;
        }

        /// <inheritdoc />
        public ICircuitBuilder AddNode(string userNodeIdentifier, string nodeTypeToken)
        {
            var nodePrototype = this.nodePrototypeRepository.TryGetINodePrototypeByToken(nodeTypeToken);

            if (nodePrototype is null)
            {
                this.errors.Add($"Attempt to add node of unknown type {nodeTypeToken}");
            }
            else
            {
                this.nodes.Add(userNodeIdentifier, nodePrototype);
            }

            return this;
        }

        /// <summary>
        /// Adds a warning to the warnings list.
        /// </summary>
        /// <param name="warningText">The warning to be added.</param>
        /// <returns></returns>
        public ICircuitBuilder AddWarning(string warningText)
        {
            warnings.Add(warningText);
            return this;
        }


        /// <summary>
        /// Adds an error to the errorlist.
        /// </summary>
        /// <param name="errorText">The error to be added.</param>
        /// <returns></returns>
        public ICircuitBuilder AddError(string errorText)
        {
            errors.Add(errorText);
            return this;
        }
        
        /// <inheritdoc />
        public ICircuitBuilder ApplyInputTokens(IEnumerable<CircuitInputToken> inputTokens)
        {
            inputTokens.ForEach(x =>
            {
                switch (x)
                {
                    case EmitErrorInputToken errorT:
                        this.AddError(errorT.ErrorText);
                        break;
                    case EmitWarningInputToken warnT:
                        this.AddWarning(warnT.WarningText);
                        break;
                    case NodeDefinitionInputToken nodeT:
                        this.AddNode(nodeT.UserNodeIdentifier, nodeT.NodeTypeToken);
                        break;
                    case EdgeDefinitionInputToken edgeT:
                        this.AddEdge(edgeT.UserOutputNodeIdentifier, edgeT.UserInputNodeIdentifier);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(inputTokens), "Token is of unknown type.");
                }
            });

            return this;
        }

        /// <inheritdoc />
        public bool TryBuildCircuit(out Circuit? circuit, out IEnumerable<string> warnings, out IEnumerable<string> errors)
        {
            this.nodes.ForEach(n => ValidateNode(n.Key));
            var filteredEdges = this.edges
                .SelectMany(kvp => kvp.Value.Select(v => (origin: kvp.Key, dest: v)))
                .Where(tup => this.ValidateEdge(tup.origin, tup.dest))
                .GroupBy(tup => tup.origin)
                .ToDictionary(
                group => group.Key, 
                group => group.Select(tup => tup.dest).ToList());
            
            errors = this.errors;
            warnings = this.warnings;

            if (this.errors.Count > 0)
            {
                circuit = null;
                return false;
            }

            var nodesByUserId = new Dictionary<string, Node>();
            var nodeIdsToDo = filteredEdges.Keys.Concat(filteredEdges.SelectMany(x => x.Value)).Distinct().ToImmutableList();

            while (!nodeIdsToDo.IsEmpty)
            {
                var mutableNodeIds = nodeIdsToDo.ToBuilder();

                nodeIdsToDo.ForEach(nodeId =>
                {
                    // If the node does not have any outputs or all outputs have been constructed,
                    // the node can be constructed.

                    if (
                        !filteredEdges.TryGetValue(nodeId, out var outputs)
                        || outputs.All(nodeId => nodesByUserId.ContainsKey(nodeId)))
                    {
                        nodesByUserId.Add(
                            nodeId,
                            new Node(
                                Guid.NewGuid(),
                                nodeId,
                                nodes[nodeId],
                                outputs?.Select(edge => new Edge(Guid.NewGuid(), nodesByUserId[edge])).ToImmutableList()
                                    ?? ImmutableList<Edge>.Empty));
                        mutableNodeIds.Remove(nodeId);
                    }
                });

                if (nodeIdsToDo.Count == mutableNodeIds.Count)
                {
                    // Infinite loop, emit error.
                    this.errors.Add("Detected infinite loop in circuit.");
                    break;
                }

                nodeIdsToDo = mutableNodeIds.ToImmutable();
            }

            if (this.errors.Any())
            {
                circuit = default;
                return false;
            }

            circuit = new Circuit(
                nodesByUserId
                    .Where(x => x.Value.Prototype is InputNodePrototypeBase)
                    .ToImmutableDictionary(x => x.Value.Id, x => x.Value),
                nodesByUserId
                    .Where(x => x.Value.Prototype is OutputNodePrototype)
                    .ToImmutableDictionary(x => x.Value.Id, x => x.Value));

            return true;
        }

        private void ValidateNode(string userNodeId)
        {
            var prototype = this.nodes[userNodeId];

            var prependedNodes = edges.Where(x => x.Value.Contains(userNodeId)).Select(x => x.Key).ToList();

            if (prependedNodes.Count > prototype.MaxInputs)
            {
                this.AddError($"Node '{userNodeId}' has too many inputs.");
            }
            else if (prependedNodes.Count < prototype.MinInputs) 
            {
                this.AddError($"Node '{userNodeId}' has too few inputs.");
            }

            var outputCount = 0;

            if (edges.TryGetValue(userNodeId, out var outputs))
            {
                outputCount = outputs.Count;
            }

            if (outputCount > prototype.MaxOutputs)
            {
                this.AddError($"Node '{userNodeId}' has too many outputs.");
            }
            else if (outputCount < prototype.MinOutputs)
            {
                this.AddError($"Node '{userNodeId}' has too few outputs.");
            }
        }

        private bool ValidateEdge(string originNodeId, string targetNodeId)
        {
            var rv = true;
            if (!this.nodes.ContainsKey(originNodeId))
            {
                this.AddWarning($"Attempted to construct an edge between '{originNodeId}' and '{targetNodeId}', " +
                                $"where '{originNodeId}' does not exist.");
                rv = false;
            }
            if (!this.nodes.ContainsKey(targetNodeId))
            {
                this.AddWarning(
                    $"Attempted to construct an edge between '{originNodeId}' and '{targetNodeId}', " +
                    $"where '{targetNodeId}' does not exist.");
                rv = false;
            }

            return rv;
        }
    }
}
