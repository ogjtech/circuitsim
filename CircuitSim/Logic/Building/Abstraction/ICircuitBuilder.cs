﻿using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.ParsingModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Building.Abstraction
{
    public interface ICircuitBuilder
    {
        /// <summary>
        /// Adds an edge to this builder.
        /// </summary>
        /// <param name="outputNodeUserNodeIdentifier">
        /// The user identifier of the node where this edge starts
        /// at an output port.
        /// </param>
        /// <param name="inputNodeUserNodeIdentifier">
        /// The user identifier of the node where this edge ends
        /// at an input port.
        /// </param>
        /// <returns>This <see cref="ICircuitBuilder"/>.</returns>
        ICircuitBuilder AddEdge(string outputNodeUserNodeIdentifier, string inputNodeUserNodeIdentifier);

        /// <summary>
        /// Adds a node to this builder.
        /// </summary>
        /// <param name="userNodeIdentifier">
        /// The identifier of the node as given by the user.
        /// </param>
        /// <param name="nodeTypeToken">
        /// The token of the node.
        /// </param>
        /// <returns>This <see cref="ICircuitBuilder"/>.</returns>
        ICircuitBuilder AddNode(
            string userNodeIdentifier,
            string nodeTypeToken);

        /// <summary>
        /// Applies a sequence of input tokens to this builder.
        /// </summary>
        /// <param name="inputTokens">
        /// The input token sequence to apply.
        /// </param>
        /// <returns>This <see cref="ICircuitBuilder"/>.</returns>
        ICircuitBuilder ApplyInputTokens(
            IEnumerable<CircuitInputToken> inputTokens);

        /// <summary>
        /// Tries to build a circuit based on the current state of this
        /// <see cref="ICircuitBuilder"/>.
        /// </summary>
        /// <param name="circuit">The built circuit, or <c>null</c>.</param>
        /// <param name="warnings">Any warnings omitted by the build process.</param>
        /// <param name="errors">Any errors omitted by the build process.</param>
        /// <returns>A value indicating whether the building of the circuit was successful.</returns>
        bool TryBuildCircuit(
            out Circuit? circuit,
            out IEnumerable<string> warnings,
            out IEnumerable<string> errors);
    }
}
