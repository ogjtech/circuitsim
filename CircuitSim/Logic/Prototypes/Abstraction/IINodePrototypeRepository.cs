﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Prototypes.Abstraction
{

    /// <summary>
    /// Specifies a repository for <see cref="INodePrototype"/> instances.
    /// </summary>
    public interface IINodePrototypeRepository
    {
        /// <summary>
        /// Retrieves the current set of tokens for which an <see cref="INodePrototype"/> is available.
        /// </summary>
        /// <param name="ct">A token that can be used to request cancellation of the operation.</param>
        /// <returns>The current set of tokens for which an <see cref="INodePrototype"/> is available.</returns>
        ImmutableHashSet<string> GetTokensOfAvailableINodePrototypes();

        /// <summary>
        /// Attempts to retrieve an <see cref="INodePrototype"/> for a given <paramref name="token"/>.
        /// </summary>
        /// <param name="token">The token to implement.</param>
        /// <param name="ct">A token that can be used to request cancellation of the operation.</param>
        /// <returns>The discovered <see cref="INodePrototype"/>, or <c>null</c>.</returns>
        INodePrototype? TryGetINodePrototypeByToken(string token);
    }
}
