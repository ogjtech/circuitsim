﻿using CircuitSim.Core;
using CircuitSim.Logic.Prototypes.Abstraction;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;

namespace CircuitSim.Logic.Prototypes.Implementation
{
    public sealed class INodePrototypeRepository : IINodePrototypeRepository
    {
        private ImmutableDictionary<string, INodePrototype> iNodePrototypesByToken
            = ImmutableDictionary<string, INodePrototype>.Empty;

        /// <inheritdoc />
        public INodePrototype? TryGetINodePrototypeByToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("May not be null or whitespace.", nameof(token));

            if (!this.iNodePrototypesByToken.TryGetValue(token, out INodePrototype? prototype))
            {
                this.LoadNodePrototypes();

                if (!this.iNodePrototypesByToken.TryGetValue(token, out prototype))
                {
                    prototype = null;
                }
            }

            return prototype;
        }

        /// <inheritdoc />
        public ImmutableHashSet<string> GetTokensOfAvailableINodePrototypes()
        {
            this.LoadNodePrototypes();
            return this.iNodePrototypesByToken.Keys.ToImmutableHashSet();
        }

        private void LoadNodePrototypes()
        { 
                var nodePrototypes = AppDomain.CurrentDomain
                            .GetAssemblies()
                            .SelectMany(a => a.GetTypes())
                            .Where(t => t.IsClass
                                  && !t.IsAbstract
                                  && t.GetInterfaces().Any(i => i.IsGenericType
                                    && i.GetGenericTypeDefinition() == typeof(INodePrototype<>)
                                    && i.GetGenericArguments().SingleOrDefault() == t)
                                  && t.IsPublic
                                  && t.IsSealed
                                  && t.GetConstructors().Any(ctor =>
                                      ctor.IsPublic &&
                                      ctor.GetParameters().Length == 0))
                            .Select(Activator.CreateInstance)
                            .Cast<INodePrototype>();

                this.iNodePrototypesByToken = this.iNodePrototypesByToken.AddRange(
                    nodePrototypes
                        .Where(n => !iNodePrototypesByToken.ContainsKey(n.Token))
                        .Select(n => new KeyValuePair<string, INodePrototype>(n.Token, n)));
        }
    }
}
