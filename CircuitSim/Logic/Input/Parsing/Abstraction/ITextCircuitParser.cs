﻿using CircuitSim.Domain.ParsingModel;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace CircuitSim.Logic.Input.Parsing.Abstraction
{
    /// <summary>
    /// Specifies an object that can parse a circuit based on lines of text input.
    /// </summary>
    public interface ITextCircuitParser
    {
        /// <summary>
        /// Parses an <see cref="IEnumerable{T}"/> of <see cref="string"/> into
        /// a sequence of <see cref="CircuitInputToken"/> objects. No validation of the circuit is performed.
        /// </summary>
        /// <param name="inputSource">The input source.</param>
        /// <param name="cancellationToken">A token that can be used to request cancellation of the operation.</param>
        /// <returns>A collection of <see cref="CircuitInputToken"/> objects.</returns>
        ImmutableList<CircuitInputToken> ParseCircuitInput(
            IEnumerable<string> input);
   }
}
