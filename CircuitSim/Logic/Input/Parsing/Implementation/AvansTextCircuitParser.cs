﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Domain.ParsingModel;
using CircuitSim.Logic.Prototypes.Abstraction;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Collections.Generic;
using CircuitSim.Logic.Input.Parsing.Abstraction;

namespace CircuitSim.Logic.Input.Parsing.Implementation
{
    /// <summary>
    /// Represents a parser that can parse circuit files from text input
    /// that applies to the Avans Circuit file specification that can be found at:
    /// http://webdictaat.aii.avans.nl/dictaten/DesignPatterns1/#/Circuits (Dutch).
    /// </summary>
    public sealed class AvansTextCircuitParser : ITextCircuitParser
    {
        private const char CommentUntilEOLChar = '#';
        private const char ExpressionSeperationChar = ':';
        private const char ExpressionEndChar = ';';
        private const char NodeIdentifierSeperationChar = ',';

        private readonly IINodePrototypeRepository nodePrototypeRepository;

        public AvansTextCircuitParser(
            IINodePrototypeRepository nodePrototypeRepository)
        {
            this.nodePrototypeRepository = nodePrototypeRepository ??
                throw new ArgumentNullException(nameof(nodePrototypeRepository));
        }

        public ImmutableList<CircuitInputToken> ParseCircuitInput(
            IEnumerable<string> input)
        {
            var nodeTokens = this.nodePrototypeRepository.GetTokensOfAvailableINodePrototypes();

            return input
                .SelectMany((line, lineIndex) => ParseLine(line, lineIndex, nodeTokens))
                .ToImmutableList();
        }

        private static IEnumerable<CircuitInputToken> ParseLine(
            string line,
            int lineIndex, 
            ImmutableHashSet<string> possibleNodeTokens)
        {
            string MakeMessage(string message) => $"[{(lineIndex + 1).ToString().PadLeft(3, '0')}] {message}";

            var current = Enumerable.Empty<CircuitInputToken>();

            var preparedLine = // Remove all whitespace and comments
                new string(line.Split(CommentUntilEOLChar).First()
                    .Where(c => !(char.IsWhiteSpace(c)))
                    .ToArray());

            if (string.IsNullOrWhiteSpace(preparedLine))
            {
                // Entire line was a commment.
                return current;
            }

            if (!(preparedLine[^1] == ExpressionEndChar))
            {
                current = current.Append(new EmitWarningInputToken(MakeMessage(
                    $"Expected '{ExpressionEndChar}' at end of line.")));
            }
            else
            {
                preparedLine = preparedLine[0..^1];
            }

            var leftAndRightExpr = preparedLine.Split(ExpressionSeperationChar);

            if (leftAndRightExpr.Length != 2)
            {
                current = current.Append(new EmitErrorInputToken(MakeMessage(
                    $"Expected 2 expressions divided by '{ExpressionSeperationChar}', found {leftAndRightExpr.Length} expression(s).")));
                return current;
            }

            var leftHand = leftAndRightExpr.First();
            var rightHand = leftAndRightExpr.Last();

            if (possibleNodeTokens.Contains(rightHand))
            {
                // Input line is a node definition.

                if (rightHand.Contains(NodeIdentifierSeperationChar))
                {
                    current = current.Append(new EmitErrorInputToken(MakeMessage(
                        $"Expected one node identifier in right-hand of expression, found multiple.")));
                }
                else
                {
                    current = current.Append(new NodeDefinitionInputToken(
                        leftHand,
                        rightHand));
                }
            }
            else
            {
                // Input line is an edge definition.
                var inputNodes = rightHand.Split(NodeIdentifierSeperationChar);

                current = current.Concat(
                    inputNodes.Select(iN => new EdgeDefinitionInputToken(
                        leftHand,
                        iN)));
            }

            return current;
        }
    }
}
