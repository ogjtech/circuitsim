﻿using CircuitSim.Logic.Input.Reading.Abstraction;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Input.Reading.Implementation
{
    /// <summary>
    /// Impl. for <see cref="IFileReader"/>.
    /// </summary>
    public class FileReader : IFileReader
    {
        /// <inheritdoc />
        public async Task<IEnumerable<string>?> TryReadFileContentAsync(string filePath, CancellationToken ct)
        {
            try
            {
                return await File
                    .ReadAllLinesAsync(
                        filePath,
                        Encoding.UTF8,
                        ct)
                    .ConfigureAwait(false);
            }
            catch (IOException ioEx)
            {
                Debug.WriteLine(ioEx);
                return null;
            }
        }
    }
}
