﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Input.Reading.Abstraction
{
    /// <summary>
    /// Specifies an object that can read the content of an UTF-8 encoded text file.
    /// </summary>
    public interface IFileReader
    {
        /// <summary>
        /// Attempts to asynchronously read the content of a file.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="ct">
        /// A <see cref="CancellationToken"/>.
        /// </param>
        /// <returns>
        /// The lines inside the file or <c>null</c> when an error occurred.
        /// </returns>
        Task<IEnumerable<string>?> TryReadFileContentAsync(string filePath, CancellationToken ct);
    }
}
