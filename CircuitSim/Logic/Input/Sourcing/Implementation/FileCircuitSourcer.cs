﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Domain.ParsingModel;
using CircuitSim.Logic.Input.Sourcing.Abstraction;
using CircuitSim.Logic.Input.Parsing.Abstraction;
using CircuitSim.Logic.Input.Reading.Abstraction;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Input.Sourcing.Implementation
{
    /// <summary>
    /// Impl. for <see cref="ICircuitSourcer{TSource}"/> that can generate <see cref="CircuitInputToken"/>
    /// objects from a <see cref="FileCircuitSource"/>.
    /// </summary>
    public class FileCircuitSourcer : ICircuitSourcer<FileCircuitSource>
    {
        private readonly IFileReader fileReader;
        private readonly ITextCircuitParser circuitParser;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileCircuitSourcer"/> class.
        /// </summary>
        /// <param name="fileReader">The file reader.</param>
        /// <param name="circuitParser">The circuit parser.</param>
        public FileCircuitSourcer(
            IFileReader fileReader,
            ITextCircuitParser circuitParser)
        {
            this.fileReader = fileReader;
            this.circuitParser = circuitParser;
        }

        /// <inheritdoc />
        public async Task<ImmutableList<CircuitInputToken>> ParseCircuitInputAsync(
            FileCircuitSource inputSource,
            CancellationToken cancellationToken)
        {
            var inputLines = await this.fileReader.TryReadFileContentAsync(
                inputSource.FilePath,
                cancellationToken);

            if (inputLines is null)
            {
                return ImmutableList.Create(new EmitErrorInputToken("Cannot read file.") as CircuitInputToken);
            }

            return this.circuitParser.ParseCircuitInput(inputLines);
        }
    }
}
