﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Domain.ParsingModel;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.Logic.Input.Sourcing.Abstraction
{
    /// <summary>
    /// Specifies an object that can generate a sequence of <see cref="CircuitInputToken"/>
    /// objects from a <see cref="CircuitSource"/>.
    /// </summary>
    /// <typeparam name="TSource">
    /// The type of source this <see cref="ICircuitSourcer{TSource}"/> can handle.
    /// </typeparam>
    public interface ICircuitSourcer<TSource>
        where TSource : CircuitSource
    {
        /// <summary>
        /// Asynchronously parses a <see cref="CircuitSource"/> into
        /// a sequence of <see cref="CircuitInputToken"/> objects. No validation on the circuit is performed.
        /// Any warnings and/or errors will be included in the output as respective <see cref="EmitWarningInputToken"/>
        /// ánd <see cref="EmitErrorInputToken"/> object.
        /// </summary>
        /// <param name="inputSource">The input source.</param>
        /// <param name="cancellationToken">A token that can be used to request cancellation of the operation.</param>
        /// <returns>A collection of <see cref="CircuitInputToken"/> objects.</returns>
        Task<ImmutableList<CircuitInputToken>> ParseCircuitInputAsync(
            TSource inputSource,
            CancellationToken cancellationToken);
    }
}
