﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CircuitSim.App
{
    public class Program
    {
        private static readonly string PluginFolderName = "Plugins";

        public static async Task Main()
        {
            // Load any plugins present in the plugins folder into the app domain.
            Directory.GetFiles(PluginFolderName, "*.dll")
                .ForEach(asFName => Assembly.LoadFrom(asFName));

            // Run the app.
            try
            {
                await CreateHostBuilder()
                  .RunConsoleAsync();
            }
            catch (OperationCanceledException)
            {
                // Ignore. Internal app shutdown requested.
            }
        }

        private static IHostBuilder CreateHostBuilder()
        {
            var builder = Host.CreateDefaultBuilder();

            builder.ConfigureLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.AddDebug();
            });

            builder.ConfigureServices(ConfigureServices);

            return builder;
        }

        private static void ConfigureServices(HostBuilderContext hbc, IServiceCollection svc)
        {
            Core.ServiceRegistrations.ConfigureServices(hbc, svc);
            Domain.ServiceRegistrations.ConfigureServices(hbc, svc);
            Data.ServiceRegistrations.ConfigureServices(hbc, svc);
            Logic.ServiceRegistrations.ConfigureServices(hbc, svc);
            View.ServiceRegistrations.ConfigureServices(hbc, svc);
        }
    }
}
