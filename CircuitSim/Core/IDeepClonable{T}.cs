﻿namespace CircuitSim.Core
{
    /// <summary>
    /// Specifies deep-cloning operations for a type.
    /// </summary>
    /// <typeparam name="T">The type.</typeparam>
    public interface IDeepClonable<out T>
    {
        /// <summary>
        /// Creates a deep clone of this object.
        /// </summary>
        /// <returns>The deeply cloned object.</returns>
        T DeepClone();
    }
}
