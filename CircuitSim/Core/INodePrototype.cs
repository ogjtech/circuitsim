﻿using System.Collections.Immutable;
using System.Diagnostics.Contracts;

namespace CircuitSim.Core
{
    /// <summary>
    /// Specifies a node type.
    /// </summary>
    public interface INodePrototype
    {
        /// <summary>
        /// The token of the node, as used in inputs.
        /// </summary>
        /// <remarks>
        /// Note to implementers: this value <b>MUST</b> be constant
        /// over the lifetime of an instance of the class.
        /// </remarks>
        string Token { get; }

        /// <summary>
        /// The friendly name of the node, to be shown in a 
        /// user interface.
        /// </summary>
        /// <remarks>
        /// Note to implementers: this value <b>MUST</b> be constant
        /// over the lifetime of an instance of the class.
        /// </remarks>
        string FriendlyName { get; }

        /// <summary>
        /// The minimum amount of edges that must be connected
        /// to the inputs of this node. May not be smaller
        /// than zero.
        /// </summary>
        /// <remarks>
        /// Note to implementers: this value <b>MUST</b> be constant
        /// over the lifetime of an instance of the class.
        /// </remarks>
        int MinInputs { get; }

        /// <summary>
        /// The maximum amount of edges that may be connected
        /// to the inputs of this node. When <c>null</c>, an unlimited
        /// amount of edges may be connected to the inputs. May not be smaller
        /// than zero.
        /// </summary>
        /// <remarks>
        /// Note to implementers: this value <b>MUST</b> be constant
        /// over the lifetime of an instance of the class.
        /// </remarks>
        int? MaxInputs { get; }

        /// <summary>
        /// The minimum amount of edges that must be connected
        /// to the outputs of this node. May not be smaller
        /// than zero.
        /// </summary>
        /// <remarks>
        /// Note to implementers: this value <b>MUST</b> be constant
        /// over the lifetime of an instance of the class.
        /// </remarks>
        int MinOutputs { get; }

        /// <summary>
        /// The maximum amount of edges that may be connected
        /// to the outputs of this node. When <c>null</c>, an unlimited
        /// amount of edges may be connected to the outputs. May not
        /// be smaller than zero.
        /// </summary>
        /// <remarks>
        /// Note to implementers: this value <b>MUST</b> be constant
        /// over the lifetime of an instance of the class.
        /// </remarks>
        int? MaxOutputs { get; }

        /// <summary>
        /// Calculates the outputs for the node, given its inputs.
        /// </summary>
        /// <param name="inputs">The inputs of the node.</param>
        /// <param name="amountOfOutputs">The amount of outputs that are connected.</param>
        /// <returns>The outputs of the node.</returns>
        /// <remarks>
        /// Note to implementers: this function must be thread-safe and not rely on
        /// any state persistence.
        /// </remarks>
        [Pure]
        ImmutableArray<bool> CalculateResult(
            ImmutableArray<bool> inputs,
            int amountOfOutputs);
    }
}
