﻿namespace CircuitSim.Core
{
    /// <summary>
    /// Specifies a node prototype.
    /// </summary>
    /// <typeparam name="TImpl">The type of the implementation.</typeparam>
    public interface INodePrototype<TImpl> : INodePrototype
        where TImpl : class, INodePrototype<TImpl>, new()
    {
    }
}
