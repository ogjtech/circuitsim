﻿using System.Collections.Generic;

namespace System.Linq
{
    /// <summary>
    /// Contains extensions for the <see cref="object"/> type.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Enumerates the <paramref name="value"/>, resulting in an <see cref="IEnumerable{T}"/>
        /// containing only one value.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>The enumerated value.</returns>
        public static IEnumerable<T> Enumerate<T>(this T value)
        {
            yield return value;
        }
    }
}
