﻿namespace CircuitSim.Core
{
    /// <summary>
    /// Specifies shallow-cloning operations for a type.
    /// </summary>
    /// <typeparam name="T">The type.</typeparam>
    public interface IShallowClonable<out T>
    {
        /// <summary>
        /// Creates a shallow clone of this object.
        /// </summary>
        /// <returns>The shallowly cloned object.</returns>
        T ShallowClone();
    }
}
