﻿using System.Collections.Generic;

namespace System.Linq
{
    /// <summary>
    /// Contains extension methods for the <see cref="IEnumerable{T}"/> type.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Enumerates the given <paramref name="source"/> <see cref="IEnumerable{T}"/>,
        /// performing an <paramref name="action"/> for each entry.
        /// </summary>
        /// <typeparam name="T">The type of the values inside <paramref name="source"/>.</typeparam>
        /// <param name="source">The source <see cref="IEnumerable{T}"/>.</param>
        /// <param name="action">The action to perform.</param>
        public static void ForEach<T>(
            this IEnumerable<T> source,
            Action<T> action)
        {
            if (action is null)
                throw new ArgumentNullException(nameof(action));

            foreach (var value in source ?? throw new ArgumentNullException(nameof(source)))
                action(value);
        }

        /// <summary>
        /// Enumerates the given <paramref name="source"/> <see cref="IEnumerable{T}"/>,
        /// performing an <paramref name="action"/> for each entry. The index of the element
        /// in the <see cref="IEnumerable{T}"/> is passed to the first argument of the action.
        /// </summary>
        /// <typeparam name="T">The type of the values inside <paramref name="source"/>.</typeparam>
        /// <param name="source">The source <see cref="IEnumerable{T}"/>.</param>
        /// <param name="action">The action to perform.</param>
        public static void ForEach<T>(
            this IEnumerable<T> source,
            Action<int, T> action)
        {
            if (action is null)
                throw new ArgumentNullException(nameof(action));

            var counter = 0;

            foreach (var value in source ?? throw new ArgumentNullException(nameof(source)))
            {
                action(counter, value);
                counter++;
            }
        }

        /// <summary>
        /// Returns a new <see cref="IEnumerable{T}"/> based on <paramref name="source"/> that,
        /// when enumerated, will perform a side effect (<paramref name="action"/>) for each
        /// value in the <paramref name="source"/>.
        /// </summary>
        /// <typeparam name="T">The type of the values inside <paramref name="source"/>.</typeparam>
        /// <param name="source">The source <see cref="IEnumerable{T}"/>.</param>
        /// <param name="action">The action to perform.</param>
        /// <returns>The new <see cref="IEnumerable{T}"/>.</returns>
        public static IEnumerable<T> Tap<T>(
            this IEnumerable<T> source,
            Action<T> action)
        {
            if (action is null)
                throw new ArgumentNullException(nameof(action));

            foreach (var value in source ?? throw new ArgumentNullException(nameof(source)))
            {
                action(value);
                yield return value;
            }
        }
    }
}
