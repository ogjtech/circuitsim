﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CircuitSim.Data
{
    public static class ServiceRegistrations
    {
        public static void ConfigureServices(HostBuilderContext hbc, IServiceCollection svc)
        {
            _ = hbc;
            _ = svc;
        }
    }
}
