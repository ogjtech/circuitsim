﻿using CircuitSim.Core;
using CircuitSim.Domain.ParsingModel;
using CircuitSim.Logic.Input.Parsing.Implementation;
using CircuitSim.Logic.Prototypes.Abstraction;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Immutable;
using System.Linq;
using Xunit;

namespace CircuitSim.Test.Logic.Input.Parsing
{
    public class AvansTextCircuitParserTest
    {
        private static readonly string[] DummyNodeTokens = Enumerable.Range(0, 2).Select(v => $"TYPE_{v}").ToArray();

        private static readonly string[] Input = new[]
        {
            /* I00 L01 */ "NODE_1: TYPE_0; #This comment should be ignored", // Emits NodeDeclaration { Name: NODE_1, Type: TYPE_0 }
            /* I01 L02 */ "# This comment should be ignored", // Emits nothing
            /* I02 L03 */ "NODE_2: TYPE_1;", // Emits NodeDeclaration { Name: NODE_2, TYPE: TYPE_1 }
            /* I03 L04 */ "NODE_3: TYPE_1 # This line should emit a syntax warning", // Emits NodeDeclaration { Name: NODE_23, TYPE: TYPE_1 }, EmitWarning { Syntax Error }
            /* I04 L05 */ "# Now lets connect the dots", // Emits nothing
            /* I05 L06 */ "NODE_1: NODE_2, NODE_3;", // Emits EdgeDefinition { Origin: NODE_1, Dest: [ NODE_2, NODE_3 ] }
            /* I06 L07 */ "NODE_2: NODE_3;", // Emits EdgeDefinition { Origin: NODE_2, Dest: [ NODE_3 ] }
            /* I07 L08 */ "THIS IS A SYNTAX ERROR;", // EmitError { Syntax Error: Not 2 expressions }
        };

        private readonly AvansTextCircuitParser parser;
        private readonly Mock<IINodePrototypeRepository> nodeRepositoryMock;

        public AvansTextCircuitParserTest()
        {
            this.nodeRepositoryMock = new Mock<IINodePrototypeRepository>();
            this.parser = new AvansTextCircuitParser(this.nodeRepositoryMock.Object);
        }

        [Fact]
        public void ParseCircuitInputTest()
        {
            this.nodeRepositoryMock.Setup(x => x.GetTokensOfAvailableINodePrototypes())
                .Returns(DummyNodeTokens.ToImmutableHashSet())
                .Verifiable();

            var tokens = this.parser.ParseCircuitInput(Input).ToList(); // Enumerable must be enumerated to invoke all logic

            tokens.Should()
                .NotBeNull(because: "returning null is not allowed in this context")
                .And
                .NotContainNulls(because: "nulls are not allowed in this context")
                .And
                .SatisfyRespectively(new Action<CircuitInputToken>[]
                {
                    // I00 L01
                        token => AssertNodeDeclaration(token, "NODE_1", "TYPE_0"),
                    // I02 L03
                        token => AssertNodeDeclaration(token, "NODE_2", "TYPE_1"),
                    // I03 L04
                        token => AssertWarningEmit(token, "[004] Expected ';' at end of line."),
                        token => AssertNodeDeclaration(token, "NODE_3", "TYPE_1"),
                    // I05 L06
                        token => AssertEdgeDeclaration(token, "NODE_1", "NODE_2"),
                        token => AssertEdgeDeclaration(token, "NODE_1", "NODE_3"),
                    // I06 LO7
                        token => AssertEdgeDeclaration(token, "NODE_2", "NODE_3"),
                    // I07 L08
                        token => AssertErrorEmit(
                            token,
                            "[008] Expected 2 expressions divided by ':', found 1 expression(s)."),
                });

            this.nodeRepositoryMock.Verify();
            this.nodeRepositoryMock.VerifyNoOtherCalls();
        }

        private static void AssertNodeDeclaration(CircuitInputToken token, string userNodeIdentifier, string nodeTypeToken)
        {
            token.Should().BeOfType<NodeDefinitionInputToken>(because: "a node is declared by the input");
            var nodeToken = token as NodeDefinitionInputToken;
            nodeToken!.UserNodeIdentifier.Should().Be(userNodeIdentifier, because: "this is the declared node name");
            nodeToken!.NodeTypeToken.Should().Be(nodeTypeToken, because: "this is the declared node type");
        }

        private static void AssertEdgeDeclaration(CircuitInputToken token, string originNode, string destinationNode)
        {
            token.Should().BeOfType<EdgeDefinitionInputToken>(because: "an edge is declared by the input");
            var edgeToken = token as EdgeDefinitionInputToken;
            edgeToken!.UserOutputNodeIdentifier.Should().Be(originNode, because: "this is the declared output node");
            edgeToken!.UserInputNodeIdentifier.Should().Be(destinationNode, because: "this is the declared input node");
        }

        private static void AssertWarningEmit(CircuitInputToken token, string expectedWarning)
        {
            token.Should().BeOfType<EmitWarningInputToken>(because: "a warning must be emitted");
            var warningToken = token as EmitWarningInputToken;
            warningToken!.WarningText.Should().Be(expectedWarning, because: "this warning must be emitted");
        }
        private static void AssertErrorEmit(CircuitInputToken token, string expectedError)
        {
            token.Should().BeOfType<EmitErrorInputToken>(because: "an error must be emitted");
            var errorToken = token as EmitErrorInputToken;
            errorToken!.ErrorText.Should().Be(expectedError, because: "this error must be emitted");
        }

        private class DummyPrototype : INodePrototype
        {
            public DummyPrototype(string token)
                => this.Token = token;

            public string Token { get; }

            public string FriendlyName => Token;

            public int MinInputs => throw new NotImplementedException();

            public int? MaxInputs => throw new NotImplementedException();

            public int MinOutputs => throw new NotImplementedException();

            public int? MaxOutputs => throw new NotImplementedException();

            public ImmutableArray<bool> CalculateResult(ImmutableArray<bool> inputs, int amountOfOutputs)
                => throw new NotImplementedException();
        }
    }
}
