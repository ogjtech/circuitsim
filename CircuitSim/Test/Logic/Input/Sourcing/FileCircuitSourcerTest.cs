﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Domain.ParsingModel;
using CircuitSim.Logic.Input.Parsing.Abstraction;
using CircuitSim.Logic.Input.Reading.Abstraction;
using CircuitSim.Logic.Input.Sourcing.Implementation;
using FluentAssertions;
using Moq;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CircuitSim.Test.Logic.Input.Sourcing
{
    /// <summary>
    /// Test for <see cref="FileCircuitSourcer"/>.
    /// </summary>
    public class FileCircuitSourcerTest
    {
        private const string FILE_PATH = "some_fpath";
        private static readonly string[] InputText = new[]
        {
            "FOO",
            "BAR",
        };

        private static readonly CircuitInputToken[] ExpectedTokens = new[]
        {
            new NodeDefinitionInputToken(
                "SOME_NODE", "SOME_TOKEN"), // A reference equality check on this token is enough to test if the correct token has been returned.
        };

        private readonly Mock<IFileReader> fileReaderMock;
        private readonly Mock<ITextCircuitParser> textParserMock;
        private readonly FileCircuitSourcer sourcer;

        public FileCircuitSourcerTest()
        {
            this.fileReaderMock = new Mock<IFileReader>();
            this.textParserMock = new Mock<ITextCircuitParser>();
            this.sourcer = new FileCircuitSourcer(fileReaderMock.Object, textParserMock.Object);
        }

        [Fact]
        public async Task ParseCircuitInputAsyncValidFileReturnsMockedTokens()
        {
            this.fileReaderMock.Setup(reader => reader.TryReadFileContentAsync(
                FILE_PATH, It.IsAny<CancellationToken>()))
                .ReturnsAsync(InputText)
                .Verifiable();

            this.textParserMock.Setup(parser => parser.ParseCircuitInput(
                It.Is<IEnumerable<string>>(x => x.SequenceEqual(InputText))))
                .Returns(ExpectedTokens.ToImmutableList())
                .Verifiable();

            var tokens = await this.sourcer.ParseCircuitInputAsync(new FileCircuitSource(FILE_PATH), default)
                .ConfigureAwait(true);

            tokens.Should()
                .NotBeNull(because: "it is not allowed to return null in this context")
                .And
                .ContainInOrder(
                    ExpectedTokens,
                    because: "the mocked tokens should be returned");

            this.VerifyMocks();
        }

        [Fact]
        public async Task ParseCircuitInputAsyncInvalidFileReturnsErrorToken()
        {
            this.fileReaderMock.Setup(reader => reader.TryReadFileContentAsync(
                FILE_PATH, It.IsAny<CancellationToken>()))
                .ReturnsAsync(null as IEnumerable<string>)
                .Verifiable();

            var tokens = await this.sourcer.ParseCircuitInputAsync(new FileCircuitSource(FILE_PATH), default)
                .ConfigureAwait(true);

            tokens.Should()
                .NotBeNull(because: "it is not allowed to return an empty collection in this context")
                .And
                .HaveCount(1, because: "a single error token should be returned")
                .And
                .SatisfyRespectively(token => token.Should()
                    .BeOfType<EmitErrorInputToken>(because: "the file was not readable so an error must be emitted"));

            this.VerifyMocks();
        }

        private void VerifyMocks()
        {
            this.fileReaderMock.Verify();
            this.fileReaderMock.VerifyNoOtherCalls();
            this.textParserMock.Verify();
            this.textParserMock.VerifyNoOtherCalls();
        }
    }
}
