﻿using CircuitSim.Logic.Input.Reading.Implementation;
using FluentAssertions;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace CircuitSim.Test.Logic.Input.Reading
{
    /// <summary>
    /// Test for <see cref="FileReader"/>.
    /// </summary>
    public class FileReaderTest
    {
        private static readonly string[] ExpectedText = new[]
        {
            "THIS",
            "IS",
            "UTF-8 ENCODED",
            "TEXT"
        };

        private static readonly string FilePath = Path.Combine(
            Directory.GetCurrentDirectory(),
            "TestArtifacts",
            "UTF8Text.txt");

        private static readonly string NonExistingFilePath = Path.Combine(
            FilePath,
            "DoesNotExist");

        private readonly FileReader reader = new FileReader();

        [Fact]
        public async Task TryReadFileContentAsyncExistingFileReturnsTextInFile()
        {
            var content = await this.reader.TryReadFileContentAsync(FilePath, default).ConfigureAwait(true);

            content.Should()
                .NotBeNull(because: "the file exists")
                .And
                .BeEquivalentTo(ExpectedText, because: "this is the content of the file");
        }

        [Fact]
        public async Task TryReadFileContentAsyncNonExistingFileReturnsNull()
        {
            var content = await this.reader.TryReadFileContentAsync(NonExistingFilePath, default).ConfigureAwait(true);

            content.Should()
                .BeNull(because: "the file does not exist");
        }
    }
}
