using System.Collections.Generic;
using System.Collections.Immutable;
using CircuitSim.NodesPlugin;
using Xunit;

namespace CircuitSim.Test.NodesPlugin
{
    public class AndGateNodePrototypeTest
    {
        [Theory]
        [MemberData(nameof(GetTestData))]
        public void AndGateTruthTableTheory(GateTestCase testCase)
        {
            GateTestLogic.RunGateTest(testCase, new AndGateNodePrototype());
        }

        public static IEnumerable<object[]> GetTestData()
        {
            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { false, false }.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { false }.ToImmutableArray(),
                },
            };
            
            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { false, true }.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { false }.ToImmutableArray(),
                },
            };
            
            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { true, false }.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { false }.ToImmutableArray(),
                },
            };
            
            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { true, true }.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { true }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { true, true, true }.ToImmutableArray(),
                    ExpectedOutputCount = 2,
                    ExpectedOutput = new[] { true, true }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { true, false, true }.ToImmutableArray(),
                    ExpectedOutputCount = 2,
                    ExpectedOutput = new[] { false, false }.ToImmutableArray(),
                },
            };
        }
    }
}