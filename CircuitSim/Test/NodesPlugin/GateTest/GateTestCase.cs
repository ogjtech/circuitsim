using System.Collections.Immutable;

namespace CircuitSim.Test.NodesPlugin
{
    public struct GateTestCase
    {
        public ImmutableArray<bool> Input { get; set; }
        
        public int ExpectedOutputCount { get; set; }
        
        public ImmutableArray<bool> ExpectedOutput { get; set; }
    }
}