using CircuitSim.Core;
using System.Linq;
using Xunit;

namespace CircuitSim.Test.NodesPlugin
{
    public static class GateTestLogic
    {
        public static void RunGateTest(GateTestCase testCase, INodePrototype target)
        {
            var output = target.CalculateResult(testCase.Input, testCase.ExpectedOutputCount);

            Assert.Equal(testCase.ExpectedOutputCount, output.Length);
            Assert.True(testCase.ExpectedOutput.SequenceEqual(output));
        }
    }
}