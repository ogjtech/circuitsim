using System.Collections.Generic;
using System.Collections.Immutable;
using CircuitSim.NodesPlugin;
using Xunit;

namespace CircuitSim.Test.NodesPlugin
{
    public class NotGateNodePrototypeTest
    {
        [Theory]
        [MemberData(nameof(GetTestData))]
        public void NotGateTruthTableTheory(GateTestCase testCase)
        {
            GateTestLogic.RunGateTest(testCase, new NotGateNodePrototype());
        }

        public static IEnumerable<object[]> GetTestData()
        {
            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { false }.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { true }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] { true }.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] {false}.ToImmutableArray(),
                },
            };
        }
    }
}