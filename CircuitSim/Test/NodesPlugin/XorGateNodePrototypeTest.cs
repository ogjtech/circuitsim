using System.Collections.Generic;
using System.Collections.Immutable;
using CircuitSim.NodesPlugin;
using Xunit;

namespace CircuitSim.Test.NodesPlugin
{
    public class XorGateNodePrototypeTest
    {
        [Theory]
        [MemberData(nameof(GetTestData))]
        public void XorGateTruthTableTheory(GateTestCase testCase)
        {
            GateTestLogic.RunGateTest(testCase, new XorGateNodePrototype());
        }

        public static IEnumerable<object[]> GetTestData()
        {
            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] {false, false}.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { false }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] {false, true}.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { true }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] {true, false}.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { true }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] {true, true}.ToImmutableArray(),
                    ExpectedOutputCount = 1,
                    ExpectedOutput = new[] { false }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] {true, true, false}.ToImmutableArray(),
                    ExpectedOutputCount = 2,
                    ExpectedOutput = new[] { false, false }.ToImmutableArray(),
                },
            };

            yield return new object[]
            {
                new GateTestCase()
                {
                    Input = new[] {true, false, false}.ToImmutableArray(),
                    ExpectedOutputCount = 2,
                    ExpectedOutput = new[] { true, true }.ToImmutableArray(),
                },
            };
        }
    }
}