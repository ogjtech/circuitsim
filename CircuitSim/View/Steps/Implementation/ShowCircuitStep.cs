﻿using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.StateModel;
using CircuitSim.View.Steps.Abstraction;
using CircuitSim.View.Views.Abstraction;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View.Steps.Implementation
{
    /// <summary>
    /// Represents a step that shows the user the circuit ands asks if the user
    /// whishes to override any start values.
    /// </summary>
    public class ShowCircuitStep : IStep<Circuit, ImmutableDictionary<Guid, SignalState>>
    {
        private readonly IElementView<Circuit> circuitView;

        public ShowCircuitStep(
            IElementView<Circuit> circuitView)
        {
            this.circuitView = circuitView ?? throw new ArgumentNullException(nameof(circuitView));
        }

        /// <inheritdoc/>
        public ValueTask<ImmutableDictionary<Guid, SignalState>> ExecuteAsync(
            Circuit input, CancellationToken ct)
        {
            Console.WriteLine("The following circuit has been constructed from the input:");
            Console.WriteLine();
            this.circuitView.PrintElement(input);
            Console.WriteLine();
            Console.WriteLine();

            var @in = string.Empty;
            var overrides = ImmutableDictionary.CreateBuilder<Guid, SignalState>();

            do
            {
                Console.WriteLine("Should you wish to override starting values, please do so now");
                Console.WriteLine("by specifying them in the following format: '[NODE_ID]:[H|L]'.");
                Console.WriteLine("Example: 'NODE1:H'. 'H' = 'High', 'L' = 'Low'.");
                Console.WriteLine("To continue, press return.");
                Console.WriteLine("Waiting on your input...");
                @in = Console.ReadLine();
                Console.WriteLine();

                var splitted = @in.Split(':');

                if (splitted.Length != 2 || !(splitted[1].StartsWith('H') || splitted[1].StartsWith('L')))
                {
                    Console.WriteLine("Invalid input.");
                }
                else
                {
                    var node = input.InputNodes.Select(kvp => kvp.Value).FirstOrDefault(x => x.UserNodeId == splitted[0]);

                    if (node is null)
                    {
                        Console.WriteLine("Cannot find node.");
                    }
                    else
                    {
                        var signalState = splitted[1].StartsWith('H') ? SignalState.HIGH : SignalState.LOW;

                        if (overrides.ContainsKey(node.Id))
                        {
                            overrides[node.Id] = signalState;
                        }
                        else
                        {
                            overrides.Add(node.Id, signalState);
                        }

                        Console.WriteLine("Applied override.");
                    }    
                }

                Console.WriteLine();
            } while (!string.IsNullOrWhiteSpace(@in));

            return new ValueTask<ImmutableDictionary<Guid, SignalState>>(overrides.ToImmutable());
        }
    }
}
