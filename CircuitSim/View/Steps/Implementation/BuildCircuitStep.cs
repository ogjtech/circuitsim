﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.Logic.Factory.Abstraction;
using CircuitSim.View.Steps.Abstraction;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View.Steps.Implementation
{
    /// <summary>
    /// Represents a step where the circuit is built and
    /// any errors / warnings are shown to the user.
    /// </summary>
    public class BuildCircuitStep : IStep<CircuitSource, Circuit>
    {
        private readonly ICircuitFactory circuitFactory;
        private readonly IHostApplicationLifetime hostApplicationLifetime;

        /// <summary>
        /// Initializes a new instance of the <see cref="BuildCircuitStep"/> class.
        /// </summary>
        /// <param name="circuitFactory">The circuit factory.</param>
        public BuildCircuitStep(
            ICircuitFactory circuitFactory,
            IHostApplicationLifetime hostApplicationLifetime)
        {
            this.circuitFactory = circuitFactory ?? throw new ArgumentNullException(nameof(circuitFactory));
            this.hostApplicationLifetime = hostApplicationLifetime ?? throw new ArgumentNullException(nameof(hostApplicationLifetime));
        }

        /// <inheritdoc />
        public async ValueTask<Circuit> ExecuteAsync(CircuitSource input, CancellationToken ct)
        {
            Console.WriteLine("Going to build the circuit.");
            var (circuit, errors, warnings) = (await this.circuitFactory.BuildCircuit(
                input, ct).ConfigureAwait(false));

            errors.ForEach(error => Console.WriteLine($"[ERR] {error}"));
            warnings.ForEach(warning => Console.WriteLine($"[WRN] {warning}"));

            if (circuit is null)
            {
                Console.WriteLine("Errors occured when building the circuit. Press any key to continue...");
                Console.ReadKey();
                this.hostApplicationLifetime.StopApplication();
            }
            else
            {
                Console.WriteLine("Circuit has been built. Press any key to continue...");
                Console.ReadKey();
            }
            return circuit!;
        }
    }
}
