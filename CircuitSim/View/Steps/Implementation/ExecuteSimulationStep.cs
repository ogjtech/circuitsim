﻿using CircuitSim.Domain;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.StateModel;
using CircuitSim.Logic.Simulating.Abstraction;
using CircuitSim.View.Steps.Abstraction;
using CircuitSim.View.Views.Abstraction;
using System;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View.Steps.Implementation
{
    public class ExecuteSimulationStep : IStep<(
        Circuit circuit,
        ImmutableDictionary<Guid, SignalState> inputOverrides),
        CircuitSimulationResult>
    {
        private readonly ICircuitSimulator circuitSimulator;
        private readonly IElementView<(Circuit circuit, CircuitSimulationResult result)> resultView;

        public ExecuteSimulationStep(
            ICircuitSimulator circuitSimulator,
            IElementView<(Circuit circuit, CircuitSimulationResult result)> resultView)
        {
            this.circuitSimulator = circuitSimulator ?? throw new ArgumentNullException(nameof(circuitSimulator));
            this.resultView = resultView ?? throw new ArgumentNullException(nameof(resultView));
        }

        /// <inheritdoc />
        public ValueTask<CircuitSimulationResult> ExecuteAsync(
            (Circuit circuit, ImmutableDictionary<Guid, SignalState> inputOverrides) input, CancellationToken ct)
        {
            Console.WriteLine("Going to run the simulation...");

            var result = this.circuitSimulator.SimulateCircuit(input.circuit, input.inputOverrides);

            Console.WriteLine("Ran the simulation.");

            Console.WriteLine();
            this.resultView.PrintElement((input.circuit, result));
            Console.WriteLine();

            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();

            return new ValueTask<CircuitSimulationResult>(result);
        }
    }
}
