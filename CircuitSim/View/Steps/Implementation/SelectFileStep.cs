﻿using CircuitSim.Domain.ApplicationModel;
using CircuitSim.View.Steps.Abstraction;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View.Steps.Implementation
{
    /// <summary>
    /// Represents a step that asks the user to select a file.
    /// </summary>
    /// <remarks>
    /// This step ignores the input value.
    /// </remarks>
    public class SelectFileStep : IStep<int, FileCircuitSource>
    {
        /// <inheritdoc />
        public ValueTask<FileCircuitSource> ExecuteAsync(int input, CancellationToken ct)
        {
            var filePath = default(string);

            while(filePath is null)
            {
                Console.WriteLine("Please enter a file path to a circuit file...");

                var path = Console.ReadLine().Replace("\"", string.Empty).Replace("'", string.Empty);

                if (!File.Exists(path))
                {
                    Console.WriteLine($"Path '{path}' is not valid.");
                }
                else
                {
                    filePath = path;
                }
            }

            ct.ThrowIfCancellationRequested();

            return new ValueTask<FileCircuitSource>(new FileCircuitSource(filePath));
        }
    }
}
