﻿using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View.Steps.Abstraction
{
    /// <summary>
    /// Represents a step in the GUI that produces a result.
    /// </summary>
    /// <typeparam name="TResult">The result.</typeparam>
    public interface IStep<TInput, TResult>
    {
        /// <summary>
        /// Executes the step.
        /// </summary>
        /// <param name="input">The input of the step.</param>
        /// <param name="ct">A <see cref="CancellationToken"/>.</param>
        /// <returns>
        /// The result of the step.
        /// </returns>
        ValueTask<TResult> ExecuteAsync(TInput input, CancellationToken ct);
    }
}
