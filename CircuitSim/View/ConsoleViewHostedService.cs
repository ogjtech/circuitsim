﻿using CircuitSim.View.Abstraction;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View
{
    public sealed class ConsoleViewHostedService : IHostedService, IDisposable
    {
        private readonly CancellationTokenSource cancellationTokenSource;
        private readonly IMainConsoleView mainConsoleView;
        private readonly IHostApplicationLifetime appLifetime;
        private Task? consoleRunningTask;

        public ConsoleViewHostedService(
            IMainConsoleView mainConsoleView,
            IHostApplicationLifetime appLifetime)
        {
            this.cancellationTokenSource = new CancellationTokenSource();
            this.mainConsoleView = mainConsoleView ?? throw new ArgumentNullException(nameof(mainConsoleView));
            this.appLifetime = appLifetime ?? throw new ArgumentNullException(nameof(appLifetime));
        }

        /// <inheritdoc />
        public Task StartAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            this.consoleRunningTask = this.RunAndShutdown();

            if (consoleRunningTask.IsCompleted)
            {
                return consoleRunningTask;
            }

            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (this.consoleRunningTask == null)
            {
                return;
            }

            try
            {
                this.cancellationTokenSource.Cancel();
            }
            finally
            {
                try
                {
                    await Task.WhenAny(
                        this.consoleRunningTask,
                        Task.Delay(Timeout.Infinite, cancellationToken))
                        .ConfigureAwait(true);
                }
                catch (OperationCanceledException ex) when (ex.CancellationToken.Equals(this.cancellationTokenSource.Token))
                {
                }
            }
        }

        public void Dispose() => this.cancellationTokenSource?.Dispose();

        private async Task RunAndShutdown()
        {
            try
            {
                await this.mainConsoleView.RunAsync(this.cancellationTokenSource.Token).ConfigureAwait(false);
            } catch (OperationCanceledException opCEx) when (opCEx.CancellationToken == this.cancellationTokenSource.Token)
            {
                return;
            }
            this.appLifetime.StopApplication();
        }
    }
}
