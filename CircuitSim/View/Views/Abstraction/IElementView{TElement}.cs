﻿namespace CircuitSim.View.Views.Abstraction
{
    /// <summary>
    /// Represents a console view for an element of type <typeparamref name="TElement"/>.
    /// </summary>
    /// <typeparam name="TElement">The element to print out.</typeparam>
    public interface IElementView<TElement>
    {
        /// <summary>
        /// Prints the given element to the console.
        /// </summary>
        /// <param name="element">The element.</param>
        void PrintElement(TElement element);
    }
}
