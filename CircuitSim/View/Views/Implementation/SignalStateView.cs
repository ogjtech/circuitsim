﻿using CircuitSim.Domain.StateModel;
using CircuitSim.View.Views.Abstraction;
using System;

namespace CircuitSim.View.Views.Implementation
{
    public class SignalStateView : IElementView<SignalState>
    {
        public void PrintElement(SignalState element)
        {
            Console.Write(element switch
            {
                SignalState.HIGH => "High",
                SignalState.LOW => "Low",
                SignalState.NOT_EVALUATED => "N/A",
                _ => throw new ArgumentOutOfRangeException(nameof(element)),
            });
        }
    }
}
