﻿using CircuitSim.Domain.CircuitModel;
using CircuitSim.View.Views.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CircuitSim.View.Views.Implementation
{
    /// <summary>
    /// Represents the view for a <see cref="Circuit"/>.
    /// </summary>
    public class CircuitView : IElementView<Circuit>
    {
        private readonly IElementView<Node> nodeView;

        public CircuitView(
            IElementView<Node> nodeView)
        {
            this.nodeView = nodeView ?? throw new System.ArgumentNullException(nameof(nodeView));
        }

        public void PrintElement(Circuit element)
        {
            Console.WriteLine("---BEGIN CIRCUIT---");

            element.InputNodes.Values.SelectMany(FlattenNodeRecursive)
                .GroupBy(n => n.Id)
                .Select(g => g.First())
                .ForEach(node =>
                {
                    this.nodeView.PrintElement(node);
                    Console.Write(" => ");
                    node.OutputEdges
                        .Select(x => x.OutputSide)
                        .ForEach(childNode =>
                        {
                            this.nodeView.PrintElement(childNode);
                            Console.Write(", ");
                        });
                    Console.WriteLine();
                });

            Console.Write("---ENDOF CIRCUIT---");
        }

        private static IEnumerable<Node> FlattenNodeRecursive(Node node)
            => node.OutputEdges.Select(e => e.OutputSide).SelectMany(FlattenNodeRecursive).Append(node);
    }
}
