﻿using System;
using System.Linq;
using CircuitSim.Domain;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.Domain.StateModel;
using CircuitSim.View.Views.Abstraction;

namespace CircuitSim.View.Views.Implementation
{
    /// <summary>
    /// Represents a view for a <see cref="CircuitSimulationResult"/>.
    /// </summary>
    public class CircuitSimulationResultView : IElementView<(Circuit circuit, CircuitSimulationResult result)>
    {
        private readonly IElementView<Node> nodeView;
        private readonly IElementView<SignalState> signalStateView;

        public CircuitSimulationResultView(
            IElementView<Node> nodeView,
            IElementView<SignalState> signalStateView)
        {
            this.nodeView = nodeView ?? throw new ArgumentNullException(nameof(nodeView));
            this.signalStateView = signalStateView ?? throw new ArgumentNullException(nameof(signalStateView));
        }

        /// <inheritdoc />
        public void PrintElement((Circuit circuit, CircuitSimulationResult result) element)
        {
            Console.WriteLine("---BEGIN SIMRESULT---");

            Console.WriteLine("---BEGIN SIMOUT---");

            element.circuit.OutputNodes.Values.ForEach(node =>
            {
                this.nodeView.PrintElement(node);
                Console.Write(":");
                this.signalStateView.PrintElement(element.result.OutputNodesFinalState![node.Id]);
                Console.WriteLine();
            });

            Console.WriteLine("---ENDOF SIMOUT---");

            Console.WriteLine($"IRL this would take {element.result.HistoricalCircuitStates.Count * 15} nanoseconds.");

            Console.Write("---ENDOF SIMRESULT---");
        }
    }
}
