﻿using System;
using CircuitSim.Domain.CircuitModel;
using CircuitSim.View.Views.Abstraction;

namespace CircuitSim.View.Views.Implementation
{
    /// <summary>
    /// Represents a view for a <see cref="Node"/>.
    /// </summary>
    public class NodeView : IElementView<Node>
    {
        /// <inheritdoc />
        public void PrintElement(Node element)
            => Console.Write($"[{element.UserNodeId}:{element.Prototype.Token}]");
    }
}
