﻿using System;
using System.Linq;
using CircuitSim.View.Abstraction;
using CircuitSim.View.Implementation;
using CircuitSim.View.Steps.Abstraction;
using CircuitSim.View.Views.Abstraction;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CircuitSim.View
{
    public static class ServiceRegistrations
    {
        public static void ConfigureServices(HostBuilderContext hbc, IServiceCollection svc)
        {
            _ = hbc;
            svc.AddHostedService<ConsoleViewHostedService>();
            svc.AddScoped<IMainConsoleView, MainConsoleView>();
            RegisterGeneric(svc, typeof(IElementView<>));
            RegisterGeneric(svc, typeof(IStep<,>), registerConcrete: true);
        }

        private static void RegisterGeneric(IServiceCollection svc, Type genericAbstracionType, bool registerConcrete = false)
        {
            var types = typeof(ServiceRegistrations).Assembly.GetTypes()
                .Where(type =>
                    type.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition().Equals(genericAbstracionType)))
                .Select(type => (
                    svcType: type,
                    abstractType: type.GetInterfaces().First(x => x.IsGenericType && x.GetGenericTypeDefinition().Equals(genericAbstracionType))
                ));

            if (registerConcrete)
            {
                types.ForEach(type => svc.AddScoped(type.svcType));
            }
            else
            {
                types.ForEach(type => svc.AddScoped(type.abstractType, type.svcType));
            }
        }
    }
}
