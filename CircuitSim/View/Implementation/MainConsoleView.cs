﻿using CircuitSim.View.Abstraction;
using CircuitSim.View.Steps.Implementation;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View.Implementation
{
    /// <summary>
    /// Represents the main console view.
    /// </summary>
    public class MainConsoleView : IMainConsoleView
    {
        private readonly SelectFileStep selectFileStep;
        private readonly BuildCircuitStep buildCircuitStep;
        private readonly ShowCircuitStep showCircuitStep;
        private readonly ExecuteSimulationStep executeSimulationStep;

        public MainConsoleView(
            SelectFileStep selectFileStep,
            BuildCircuitStep buildCircuitStep,
            ShowCircuitStep showCircuitStep,
            ExecuteSimulationStep executeSimulationStep)
        {
            this.selectFileStep = selectFileStep ?? throw new ArgumentNullException(nameof(selectFileStep));
            this.buildCircuitStep = buildCircuitStep ?? throw new ArgumentNullException(nameof(buildCircuitStep));
            this.showCircuitStep = showCircuitStep ?? throw new ArgumentNullException(nameof(showCircuitStep));
            this.executeSimulationStep = executeSimulationStep ?? throw new ArgumentNullException(nameof(executeSimulationStep));
        }


        /// <inheritdoc />
        public async Task RunAsync(CancellationToken ct)
        {
            await Task.Yield(); // Execute asynchronously from this point.
            var input = await ExecuteStep(() => this.selectFileStep.ExecuteAsync(0, ct), ct).ConfigureAwait(false);
            var circuit = await ExecuteStep(() => this.buildCircuitStep.ExecuteAsync(input, ct), ct).ConfigureAwait(false);

            var startOver = false;
            do
            {
                var overrides = await ExecuteStep(() => this.showCircuitStep.ExecuteAsync(circuit, ct), ct).ConfigureAwait(false);
                _ = await ExecuteStep(() => this.executeSimulationStep.ExecuteAsync((circuit, overrides), ct), ct).ConfigureAwait(false);

                Console.WriteLine();
                Console.WriteLine("Execution completed. To run a new simulation, press 'y'. To exit, press any other key...");
                startOver = Console.ReadKey().KeyChar == 'y';
            }
            while (startOver);
        }

        private static async ValueTask<TResult> ExecuteStep<TResult>(Func<ValueTask<TResult>> func, CancellationToken ct)
        {
            Console.Clear();
            ct.ThrowIfCancellationRequested();
            var result = await func();
            Console.Clear();
            ct.ThrowIfCancellationRequested();
            return result;
        }
    }
}
