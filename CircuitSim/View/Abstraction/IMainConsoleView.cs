﻿using System.Threading;
using System.Threading.Tasks;

namespace CircuitSim.View.Abstraction
{
    /// <summary>
    /// Represents the main console view.
    /// </summary>
    public interface IMainConsoleView
    {
        /// <summary>
        /// Asynchronously runs the console view.
        /// </summary>
        /// <param name="ct">A token that can be used to request cancellation.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task RunAsync(CancellationToken ct);
    }
}
