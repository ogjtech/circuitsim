﻿using System;

namespace CircuitSim.Domain.ApplicationModel
{
    /// <summary>
    /// Represents a circuit source that is a file that complies
    /// to the Avans Circuit file specification that can be found at:
    /// http://webdictaat.aii.avans.nl/dictaten/DesignPatterns1/#/Circuits (Dutch).
    /// </summary>
    public sealed class FileCircuitSource : CircuitSource
    {
        public FileCircuitSource(
            string filePath)
            : base()
        {
            this.FilePath = string.IsNullOrWhiteSpace(filePath)
                ? throw new ArgumentException("May not be null or whitespace.", nameof(filePath))
                : filePath;
        }

        /// <summary>
        /// Gets the file path to the circuit file.
        /// </summary>
        public string FilePath { get; }
    }
}
