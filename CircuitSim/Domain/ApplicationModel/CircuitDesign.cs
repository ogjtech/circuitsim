﻿using System;

namespace CircuitSim.Domain.ApplicationModel
{
    /// <summary>
    /// Represents the design of a circuit.
    /// </summary>
    public class CircuitDesign
    {
        public CircuitDesign(
            CircuitSource circuitSource)
        {
            this.CircuitSource = circuitSource ?? throw new ArgumentNullException(nameof(circuitSource));
        }

        /// <summary>
        /// Gets the source of the circuit design.
        /// </summary>
        public CircuitSource CircuitSource { get; }
    }
}
