﻿namespace CircuitSim.Domain.ApplicationModel
{
    /// <summary>
    /// Represents the source for a circuit design.
    /// </summary>
    public abstract class CircuitSource
    {
        protected internal CircuitSource()
        {
        }
    }
}
