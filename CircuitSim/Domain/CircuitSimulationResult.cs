﻿using CircuitSim.Domain.StateModel;
using System;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.Domain
{
    /// <summary>
    /// Represents the result of the simulation of a circuit.
    /// </summary>
    public class CircuitSimulationResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitSimulationResult"/> class.
        /// </summary>
        /// <param name="historicalCircuitStates">The historical circuit states.</param>
        public CircuitSimulationResult(
            ImmutableList<CircuitState> historicalCircuitStates)
        {
            this.HistoricalCircuitStates = historicalCircuitStates ?? throw new ArgumentNullException(nameof(historicalCircuitStates));
        }

        /// <summary>
        /// Gets a list of all circuit states that occured when
        /// evaluating the circuit.
        /// </summary>
        public ImmutableList<CircuitState> HistoricalCircuitStates { get; }

        /// <summary>
        /// Gets a dictionary containing the states of the output nodes at the final
        /// state.
        /// </summary>
        /// <remarks>
        /// This is equivalent to retrieving the last state and inspecting the
        /// output node states.
        /// </remarks>
        public ImmutableDictionary<Guid, SignalState>? OutputNodesFinalState =>
            HistoricalCircuitStates
                .LastOrDefault()
                ?.OutputNodeStates;
    }
}
