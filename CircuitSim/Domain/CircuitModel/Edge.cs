using System;

namespace CircuitSim.Domain.CircuitModel
{
    /// <summary>
    /// Represents a connection between two <see cref="Node"/> instances.
    /// </summary>
    public sealed class Edge
    {
        /// <summary>
        /// Gets the identifier of this <see cref="Edge"/>.
        /// </summary>
        public Guid Identifier { get; }

        /// <summary>
        /// Gets the node at the edge's output.
        /// </summary>
        public Node OutputSide { get; }

        public Edge(
            Guid identifier, Node outputSide)
        {
            this.Identifier = identifier;
            this.OutputSide = outputSide ?? throw new ArgumentNullException(nameof(outputSide));
        }
    }
}
