using CircuitSim.Core;
using System;
using System.Collections.Immutable;

namespace CircuitSim.Domain.CircuitModel
{
    /// <summary>
    /// Represents a node in the circuit.
    /// </summary>
    public sealed class Node
    {
        public Node(
            Guid id,
            string userId,
            INodePrototype prototype,
            ImmutableList<Edge> outputEdges)
        {
            this.Id = id;
            this.UserNodeId = string.IsNullOrWhiteSpace(userId)
                ? throw new ArgumentException("May not be null or whitespace.", nameof(userId))
                : userId;
            this.Prototype = prototype
                ?? throw new ArgumentNullException(nameof(prototype));
            this.OutputEdges = outputEdges
                ?? throw new ArgumentNullException(nameof(outputEdges));
        }

        /// <summary>
        /// The unique identifier of this node.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// The node identifier to be displayed in a UI.
        /// </summary>
        public string UserNodeId { get; }

        /// <summary>
        /// Gets the prototype of this node.
        /// </summary>
        public INodePrototype Prototype { get; }

        /// <summary>
        /// Gets the edges connected to the output of this node.
        /// </summary>
        public ImmutableList<Edge> OutputEdges { get; }
    }
}
