﻿using System;
using System.Collections.Immutable;

namespace CircuitSim.Domain.CircuitModel
{
    /// <summary>
    /// Represents a circuit made of nodes and edges.
    /// </summary>
    public sealed class Circuit
    {
        public Circuit(
            ImmutableDictionary<Guid, Node> inputNodes,
            ImmutableDictionary<Guid, Node> outputNodes)
        {
            this.InputNodes = inputNodes ?? throw new ArgumentNullException(nameof(inputNodes));
            this.OutputNodes = outputNodes ?? throw new ArgumentNullException(nameof(outputNodes));
        }

        /// <summary>
        /// Gets the input nodes of the circuit.
        /// </summary>
        public ImmutableDictionary<Guid, Node> InputNodes { get; }

        /// <summary>
        /// Gets the output nodes of the circuit.
        /// </summary>
        public ImmutableDictionary<Guid, Node> OutputNodes { get; }
    }
}
