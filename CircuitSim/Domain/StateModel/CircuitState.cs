﻿using System;
using System.Collections.Immutable;

namespace CircuitSim.Domain.StateModel
{
    public sealed class CircuitState
    {
        public CircuitState(
            ImmutableDictionary<Guid, SignalState> edgeStates,
            ImmutableDictionary<Guid, SignalState> outputNodeStates)
        {
            this.EdgeStates = edgeStates ?? throw new ArgumentNullException(nameof(edgeStates));
            this.OutputNodeStates = outputNodeStates ?? throw new ArgumentNullException(nameof(edgeStates));
        }

        /// <summary>
        /// Gets the states of all the edges in the circuit, keyed by the edge identifiers.
        /// </summary>
        public ImmutableDictionary<Guid, SignalState> EdgeStates { get; }

        /// <summary>
        /// Gets the states of all the output nodes in the circuit, keyed by the output node
        /// identifiers.
        /// </summary>
        public ImmutableDictionary<Guid, SignalState> OutputNodeStates { get; }

        /// <summary>
        /// Clones this <see cref="CircuitState"/>, replacing properties
        /// with supplied parameters that are not <c>null</c>.
        /// </summary>
        /// <param name="newEdgeStates">The new edge states.</param>
        /// <param name="newOutputNodeStates">The new output node states.</param>
        /// <returns>The new <see cref="CircuitState"/>.</returns>
        public CircuitState With(
            ImmutableDictionary<Guid, SignalState>? newEdgeStates = null,
            ImmutableDictionary<Guid, SignalState>? newOutputNodeStates = null) =>
            new CircuitState(
                newEdgeStates ?? this.EdgeStates,
                newOutputNodeStates ?? this.OutputNodeStates);
    }
}
