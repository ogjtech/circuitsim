﻿namespace CircuitSim.Domain.StateModel
{
    /// <summary>
    /// Represents the state of an electrical signal.
    /// </summary>
    public enum SignalState
    {
        /// <summary>
        /// The state of the signal has not yet been evaluated.
        /// </summary>
        NOT_EVALUATED = 0,

        /// <summary>
        /// The state of the signal has been evaluated to 'LOW'.
        /// </summary>
        LOW = 1,

        /// <summary>
        /// The state of the signal has been evaluated to 'HIGH'.
        /// </summary>
        HIGH =2
    }
}
