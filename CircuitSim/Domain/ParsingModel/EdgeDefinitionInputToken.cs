﻿using System;

namespace CircuitSim.Domain.ParsingModel
{
    /// <summary>
    /// Represents a <see cref="CircuitInputToken"/> that defines a connection
    /// between two nodes that may or may not have already been declared, but are
    /// guaranteed to be declared after all tokens have been processed (given that the user input is valid).
    /// </summary>
    public sealed class EdgeDefinitionInputToken : CircuitInputToken
    {
        /// <inheritdoc />
        public override CircuitInputTokenType Type => CircuitInputTokenType.EdgeDefinitionInputToken;

        /// <summary>
        /// Initializes a new instance of the <see cref="EdgeDefinitionInputToken"/> class.
        /// </summary>
        /// <param name="userOutputNodeIdentifier">The value for <see cref="UserOutputNodeIdentifier"/>.</param>
        /// <param name="userInputNodeIdentifier">The value for <see cref="UserInputNodeIdentifier"/>.</param>
        public EdgeDefinitionInputToken(
            string userOutputNodeIdentifier,
            string userInputNodeIdentifier)
        {
            if (string.IsNullOrWhiteSpace(userOutputNodeIdentifier))
                throw new ArgumentException("May not be null or whitespace.", nameof(userOutputNodeIdentifier));

            if (string.IsNullOrWhiteSpace(userInputNodeIdentifier))
                throw new ArgumentException("May not be null or whitespace.", nameof(userInputNodeIdentifier));

            this.UserOutputNodeIdentifier = userOutputNodeIdentifier;
            this.UserInputNodeIdentifier = userInputNodeIdentifier;
        }

        /// <summary>
        /// Gets the user identifier of the node to which the output
        /// of the node is connected to the input of the edge declared
        /// by this <see cref="EdgeDefinitionInputToken"/>.
        /// </summary>
        public string UserOutputNodeIdentifier { get; }

        /// <summary>
        /// Gets the user identifier of the node to which the input
        /// of the node is connected to the output of the edge declared
        /// by this <see cref="EdgeDefinitionInputToken"/>.
        /// </summary>
        public string UserInputNodeIdentifier { get; }
    }
}
