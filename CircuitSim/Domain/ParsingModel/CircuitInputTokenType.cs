﻿namespace CircuitSim.Domain.ParsingModel
{
    /// <summary>
    /// Represents the type of a <see cref="CircuitInputToken"/>.
    /// </summary>
    public enum CircuitInputTokenType
    {
        /// <summary>
        /// The token is a <see cref="ParsingModel.NodeDefinitionInputToken"/>.
        /// </summary>
        NodeDefinitionInputToken = 0,

        /// <summary>
        /// The token is a <see cref="ParsingModel.EdgeDefinitionInputToken"/>.
        /// </summary>
        EdgeDefinitionInputToken = 1,

        /// <summary>
        /// The token is a <see cref="ParsingModel.EmitWarningInputToken"/>.
        /// </summary>
        EmitWarningInputToken = 2,

        /// <summary>
        /// The token is a <see cref="ParsingModel.EmitErrorInputToken"/>.
        /// </summary>
        EmitErrorInputToken = 3,
    }
}
