﻿using System;

namespace CircuitSim.Domain.ParsingModel
{
    /// <summary>
    /// Represents an input token that causes an error to be emitted at circuit-build time,
    /// and the circuit build to fail.
    /// </summary>
    public sealed class EmitErrorInputToken : CircuitInputToken
    {
        /// <inheritdoc />
        public override CircuitInputTokenType Type => CircuitInputTokenType.EmitErrorInputToken;

        /// <summary>
        /// Initalizes a new instance of the <see cref="EmitErrorInputToken"/> class.
        /// </summary>
        /// <param name="errorText">The value for <see cref="ErrorText"/>.</param>
        public EmitErrorInputToken(
            string errorText)
        {
            if (string.IsNullOrWhiteSpace(errorText))
                throw new ArgumentException("May not be null or whitespace.", nameof(errorText));

            this.ErrorText = errorText;
        }

        /// <summary>
        /// Gets the error text that is to be emitted at circuit-build time.
        /// </summary>
        public string ErrorText { get; }
    }
}
