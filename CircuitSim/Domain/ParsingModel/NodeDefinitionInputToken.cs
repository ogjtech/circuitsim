﻿using System;

namespace CircuitSim.Domain.ParsingModel
{
    /// <summary>
    /// Represents a <see cref="CircuitInputToken"/> that
    /// defines a new node.
    /// </summary>
    public sealed class NodeDefinitionInputToken : CircuitInputToken
    {
        /// <inheritdoc />
        public override CircuitInputTokenType Type => CircuitInputTokenType.NodeDefinitionInputToken;

        /// <summary>
        /// Initializes a new instance of the <see cref="NodeDefinitionInputToken"/> class.
        /// </summary>
        /// <param name="userNodeIdentifier">The value for <see cref="UserNodeIdentifier"/>.</param>
        /// <param name="nodeTypeToken">The value for <see cref="NodeTypeToken"/>.</param>
        public NodeDefinitionInputToken(
            string userNodeIdentifier,
            string nodeTypeToken)
        {
            if (string.IsNullOrWhiteSpace(userNodeIdentifier))
                throw new ArgumentException("May not be null or whitespace.", nameof(userNodeIdentifier));

            if (string.IsNullOrWhiteSpace(nodeTypeToken))
                throw new ArgumentException("May not be null or whitespace.", nameof(nodeTypeToken));

            this.UserNodeIdentifier = userNodeIdentifier;
            this.NodeTypeToken = nodeTypeToken;
        }

        /// <summary>
        /// Gets the user identifier of the node.
        /// </summary>
        public string UserNodeIdentifier { get; }

        /// <summary>
        /// Gets the token of the node type.
        /// </summary>
        public string NodeTypeToken { get; }
    }
}
