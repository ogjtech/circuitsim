﻿using System;

namespace CircuitSim.Domain.ParsingModel
{
    /// <summary>
    /// Represents an input token that causes a warning to be emitted at circuit-build time.
    /// </summary>
    public sealed class EmitWarningInputToken : CircuitInputToken
    {
        /// <inheritdoc />
        public override CircuitInputTokenType Type => CircuitInputTokenType.EmitWarningInputToken;

        /// <summary>
        /// Initalizes a new instance of the <see cref="EmitWarningInputToken"/> class.
        /// </summary>
        /// <param name="warningText">The value for <see cref="WarningText"/>.</param>
        public EmitWarningInputToken(
            string warningText)
        {
            if (string.IsNullOrWhiteSpace(warningText))
                throw new ArgumentException("May not be null or whitespace.", nameof(warningText));

            this.WarningText = warningText;
        }

        /// <summary>
        /// Gets the warning text that is to be emitted at circuit-build time.
        /// </summary>
        public string WarningText { get; }
    }
}
