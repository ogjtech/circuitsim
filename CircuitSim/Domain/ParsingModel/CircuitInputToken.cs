﻿namespace CircuitSim.Domain.ParsingModel
{
    /// <summary>
    /// Represents an input token of a circuit source.
    /// </summary>
    public abstract class CircuitInputToken
    {
        /// <summary>
        /// Gets the type of this <see cref="CircuitInputToken"/>.
        /// </summary>
        public abstract CircuitInputTokenType Type { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitInputToken"/> class.
        /// </summary>
        internal CircuitInputToken() { }
    }
}
