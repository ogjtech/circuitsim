﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.NodesPlugin
{
    /// <summary>
    /// Impl. for the NOT gate.
    /// </summary>
    public sealed class NotGateNodePrototype : INodePrototype<NotGateNodePrototype>
    {
        /// <inheritdoc />
        public string Token => "NOT";

        /// <inheritdoc />
        public string FriendlyName => Token;

        /// <inheritdoc />
        public int MinInputs => 1;

        /// <inheritdoc />
        public int? MaxInputs => 1;

        /// <inheritdoc />
        public int MinOutputs => 1;

        /// <inheritdoc />
        public int? MaxOutputs => null;

        /// <inheritdoc />
        public ImmutableArray<bool> CalculateResult(ImmutableArray<bool> inputs, int outputCount)
        {
            var result = !inputs.Single();
            return Enumerable.Range(0, outputCount).Select(_ => result).ToImmutableArray();
        }
    }
}
