﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.NodesPlugin
{
    /// <summary>
    /// Impl. for the OR gate.
    /// </summary>
    public sealed class OrGateNodePrototype : NodePrototypeWithUnlimitedInputsAndUnlimitedOutputsBase, INodePrototype<OrGateNodePrototype>
    {
        /// <inheritdoc />
        public override string Token => "OR";

        /// <inheritdoc />
        public override bool CalculateResult(ImmutableArray<bool> inputs)
            => inputs.Any(x => x);
    }
}
