﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.NodesPlugin
{
    /// <summary>
    /// Impl. for the NAND gate.
    /// </summary>
    public sealed class NandGateNodePrototype : NodePrototypeWithUnlimitedInputsAndUnlimitedOutputsBase, INodePrototype<NandGateNodePrototype>
    {
        /// <inheritdoc />
        public override string Token => "NAND";

        /// <inheritdoc />
        public override bool CalculateResult(ImmutableArray<bool> inputs)
            => !inputs.All(x => x);
    }
}
