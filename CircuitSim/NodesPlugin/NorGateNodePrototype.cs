﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.NodesPlugin
{
    /// <summary>
    /// Impl. for the NOR gate.
    /// </summary>
    public sealed class NorGateNodePrototype : NodePrototypeWithUnlimitedInputsAndUnlimitedOutputsBase, INodePrototype<NorGateNodePrototype>
    {
        /// <inheritdoc />
        public override string Token => "NOR";

        /// <inheritdoc />
        public override bool CalculateResult(ImmutableArray<bool> inputs)
            => (!inputs.Any(x => x) && inputs.Any(x => !x));
    }
}
