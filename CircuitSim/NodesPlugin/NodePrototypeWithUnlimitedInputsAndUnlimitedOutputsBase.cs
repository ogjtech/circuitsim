﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.NodesPlugin
{
    /// <summary>
    /// Abstract base class for nodes that have unlimited inputs and unlimited outputs.
    /// The output is calculated as single value and then replicated over all output ports.
    /// </summary>
    public abstract class NodePrototypeWithUnlimitedInputsAndUnlimitedOutputsBase : INodePrototype
    {
        /// <inheritdoc />
        public abstract string Token { get; }

        /// <inheritdoc />
        public string FriendlyName => Token;


        /// <inheritdoc />
        public int MinInputs => 2;


        /// <inheritdoc />
        public int? MaxInputs => null;


        /// <inheritdoc />
        public int MinOutputs => 0;

        /// <inheritdoc />
        public int? MaxOutputs => null;

        /// <inheritdoc />
        public ImmutableArray<bool> CalculateResult(ImmutableArray<bool> inputs, int amountOfOutputs)
        {
            var result = this.CalculateResult(inputs);
            return Enumerable.Range(0, amountOfOutputs).Select(_ => result).ToImmutableArray();
        }

        /// <summary>
        /// Calculates the result. It will be "broadcasted" over all outputs.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <returns>The value for all outputs.</returns>
        public abstract bool CalculateResult(ImmutableArray<bool> inputs);
    }
}
