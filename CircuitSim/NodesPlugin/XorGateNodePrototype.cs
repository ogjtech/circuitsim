﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.NodesPlugin
{
    /// <summary>
    /// Impl. for the XOR gate.
    /// </summary>
    public sealed class XorGateNodePrototype : NodePrototypeWithUnlimitedInputsAndUnlimitedOutputsBase, INodePrototype<XorGateNodePrototype>
    {
        /// <inheritdoc />
        public override string Token => "XOR";

        /// <inheritdoc />
        public override bool CalculateResult(ImmutableArray<bool> inputs)
            =>  inputs.Where(x => x).Count() == 1;
    }
}
