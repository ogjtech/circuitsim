﻿using CircuitSim.Core;
using System.Collections.Immutable;
using System.Linq;

namespace CircuitSim.NodesPlugin
{
    /// <summary>
    /// Impl. for the AND gate.
    /// </summary>
    public sealed class AndGateNodePrototype : NodePrototypeWithUnlimitedInputsAndUnlimitedOutputsBase, INodePrototype<AndGateNodePrototype>
    {
        /// <inheritdoc />
        public override string Token => "AND";

        /// <inheritdoc />
        public override bool CalculateResult(ImmutableArray<bool> inputs)
            => inputs.All(x => x);

    }
}
